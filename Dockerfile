# Dockerfile
FROM alpine:3.15 AS downloader
ARG TEMPLATE_REPO="https://gitlab.com/DavyRoy/Test2.git"
RUN apk add git \
    && git clone ${TEMPLATE_REPO} /tmp
FROM nginx:1.23
ARG TEMPLATE_NAME="spa-dev/src"
COPY --from=downloader /tmp/${TEMPLATE_NAME} /usr/share/nginx/html